# Architect

Architect is a simple static blog generator.  Basically, it just uses two
templates to split out an index.html file that displays all of your blog posts
in a list and each blog post as a separate page.  Architect uses bootstrap and
type-a-file, so most of the styling is done automatically (especially the
typography, which is what type-a-file is great at).

## Installation

    git clone https://bitbucket.org/aweidner/architect
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt

## Use

    python architect architect [-n name] [-o output] [-c config] input_folder

There is a sample config file located in this git repo that you can edit.  Keep
in mind that if you specify a config file, all the arguments are necessary.
Here's a clarification of the arguments:

    -n = the name of your blog (use quotes to escape spaces)
    -o = output directory you want everything placed in
    -c = config file, written in pure python and imported as a module

You should use absolute filepaths for any output directories just so there isn't
any confusion.

## Special markdown

In order to integrate with type-a-file's special formatters, architect uses a
superset of markdown that looks a little like bbcode:

    [d classname]some text[/d]
    [s classname]some text[/s]

d is used for divs and s is used for spans.  Further, you can have multiple
class names and they will all be included.

    [d class1 class2]I have two classes![/d]

Read the documentation or source for type-a-file if you have any questions on
type-a-file's default classes

## Writing for architect

Architect parses the first two lines of any file in the input directory as the
title first and then the date.  These two lines HAVE to be here or architect
will throw a fit.  After that, everything is fair game.  Architect parses
markdown using the `markdown2` python module for parsing markdown, as well as
the custom syntax above.

## Customization

The templates for architect are in the `templates` folder and the css classes are
all in `style`.  Editing stylesheets is fairly straightforward and you can
include additional stylesheets if you want in the templates.

Editing the templates is a little trickier, but basically, each template gets
passed a few variables that you can mess with:

    index.html
    blog_title - the title of the blog
    posts - An array of Post objects

    post.html
    post - a single post object

Each post object has the following fields:
    
    filename - the html file that links to this post
    title - the title of the post
    date - a datetime object representing this post
    timestamp - the time in milliseconds since Jan. 1 1970 (for sorting)
    content - the content of the post

If editing the fields proves to be insufficient, architect is less than 200
lines of code.  Your best bet for doing anything really extreme is probably in
there.  Templates are standard jinja2 templates.

You can place any custom styles in the `style/default.css` file which will be
imported when any post page or the index page is rendered by the browser.

## Running the tests

Speaking of forking, architect has unit tests.  Running them is as easy as

    python test_architect.py

Feel free to add to these if you add any new features
