import time
import markdown2
import re
import dateutil.parser as dateparser

class Post(object):

    def __init__(self, filename, title, date, content):
        self.filename = filename
        self.title = title

        # The only date format it will except right now
        # is %m/%d/%y
        self.date = dateparser.parse(date)

        # timestamp used for sorting
        self.timestamp = time.mktime(self.date.timetuple())
        self.content = self.make(content)

    def make(self, content):
        return evaluate(content)

def evaluate(content):
    content = regular_markdown(content)
    content = custom_markdown(content)
    return content

def regular_markdown(content):
    return markdown2.markdown(content)

def custom_markdown(content):
    # The custom content generally comes in two forms:
    # [s classname] = <span class = "classname">content</span>
    # [d classname] = <div class = "classname">content</div>

    generic_expression_begin = r'\[%s ([-a-zA-Z0-9_ ]+)\]'
    generic_expression_end = r'\[/%s\]'
    generic_replacement_begin = r'<%s class = "\1">'
    generic_replacement_end = r'</%s>'

    div_span = [['d', 'div'], ['s', 'span']]

    for el in div_span:
        replace_starting_div = re.compile(generic_expression_begin % el[0])
        content = replace_starting_div.sub(generic_replacement_begin % el[1], content)

        replace_ending_div = re.compile(generic_expression_end % el[0])
        content = replace_ending_div.sub(generic_replacement_end % el[1], content)

    return content

