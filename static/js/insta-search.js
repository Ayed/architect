(function(jQuery) {

    $ = jQuery
    $("#query").focus()
    
    $("#query").bind('input', function() {
        text = $("#query").val()

        if (text === "") {
            $("h2").closest(".post").show();
        }
        else {
            $("h2").not(":contains('" + text + "')").closest(".post").hide()
            $("h2:contains('" + text + "')").closest(".post").show()
        }
    });

    jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
        return function( elem ) {
            return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    jQuery.expr[":"].contains = jQuery.expr.createPseudo(function(arg) {
        return function( elem ) {
            return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

})(jQuery);
