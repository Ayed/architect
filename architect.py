import os.path
import os
from jinja2 import Environment, PackageLoader
import shutil
from optparse import OptionParser

from post import Post

def _build_parser():
    parser = OptionParser("architect [-n name] [-o output] [-c config] input_folder")
    parser.add_option("-n", dest="blog_name", action="store")
    parser.add_option("-o", dest="output", action="store")
    parser.add_option("-c", dest="config", action="store")
    return parser

def make(blog_title, source, output):

    output = os.path.expanduser(output)
    source = os.path.expanduser(source)

    if not os.path.exists(output):
        os.makedirs(output)

    if not os.path.exists(source):
        print(source)
        print("Invalid source directory!")
        return

    env = Environment(loader = PackageLoader('architect', 'templates'))

    posts = []
    for root, dirnames, fnames in os.walk(source):
        for f in fnames:
            data = ""
            with open(os.path.join(root, f), "r") as fcontent:
                data = fcontent.read().split("\n")

            html_filename = os.path.splitext(f)[0] + ".html"
            posts.append(Post(html_filename, data[0], data[1], "\n".join(data[2:])))

    # sort posts by reverse chronilogical order
    posts = sorted(posts, key = lambda post: post.timestamp)
    posts.reverse()

    # Copy the stylesheets into the output folder, skip removal if it doesn't exist
    try:
        shutil.rmtree(os.path.join(output, "static"))
    except OSError, e:
        pass

    shutil.copytree("static", os.path.join(output, "static"))

    for post in posts:
        with open(os.path.join(output, post.filename), "w") as html:
            template = env.get_template('post.html')
            html.write(template.render(blog_title = blog_title, post = post))

    with open(os.path.join(output, "index.html"), "w") as index:
        template = env.get_template('index.html')
        index.write(template.render(blog_title = blog_title, posts = posts))
        

def main():
    (opts, args) = _build_parser().parse_args()

    # These variables are necessary and must be made no matter what
    blog_name = "test blog"
    output = "."
    source = "."

    try:
        config = __import__(opts.config)
        blog_name = config.blog_name
        output = config.output
        source = config.source
    except Exception:
        if opts.config:
            print("Error importing config file (don't include .py extension?), exiting")
        else:
            pass

    if opts.blog_name:
        blog_name = opts.blog_name

    if opts.output:
        output = opts.output

    if len(args) > 0:
        source = args[0]

    make(blog_name, source, output)

if __name__ == "__main__":
    main()
