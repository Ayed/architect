import unittest
import architect 
import datetime
import os.path
import post

class TestArchitect(unittest.TestCase):
    
    def test_nothing(self):
        pass

    def test_markdown(self):
        self.assertEquals(post.regular_markdown("# Hello"), u'<h1>Hello</h1>\n')

    # spans are done the same way except for s instead of d
    def test_replace_ending_div(self):
        self.assertEquals(post.custom_markdown("[/d]"), "</div>")
        self.assertEquals(post.custom_markdown("hell[/d]o"), "hell</div>o")

    def test_replace_front_div(self):
        self.assertEquals(post.custom_markdown("[d kicker]"), '<div class = "kicker">')
        self.assertEquals(post.custom_markdown("hell[d test]o"), 'hell<div class = "test">o')

    def test_replace_ending_span(self):
        self.assertEquals(post.custom_markdown("[/s]"), "</span>")
        self.assertEquals(post.custom_markdown("hell[/s]o"), "hell</span>o")

    def test_replace_front_span(self):
        self.assertEquals(post.custom_markdown("[s kicker]"), '<span class = "kicker">')
        self.assertEquals(post.custom_markdown("hell[s test]o"), 'hell<span class = "test">o')

    def test_opening_and_closing(self):
        self.assertEquals(post.custom_markdown("[s run-in]hello world[/s]"), '<span class = "run-in">hello world</span>')
        self.assertEquals(post.custom_markdown("[d kicker] testing\ntesting[/d]"), '<div class = "kicker"> testing\ntesting</div>')


    def test_custom_content(self):
       test = ["[d kicker] this is a test of the kicker[/d]",
       "[dkicker] this should not be recognized[/d]",
       "[df kicker] neither should this[/d]",
       "[d testing] should be a testing class[/d]",
       "[s test2] should be a span tag",
       "[[d kicker]]] should be wrapped in weird stuff[/d]",
       "[[d kicker]] should break stuff because first will be replaced"]

       result = ['<div class = "kicker"> this is a test of the kicker</div>',
       '[dkicker] this should not be recognized</div>',
       '[df kicker] neither should this</div>',
       '<div class = "testing"> should be a testing class</div>',
       '<span class = "test2"> should be a span tag',
       '[<div class = "kicker">]] should be wrapped in weird stuff</div>',
       '[<div class = "kicker">] should break stuff because first will be replaced']

       for i in range(len(test)):
           self.assertEquals(post.custom_markdown(test[i]), result[i])

    # Wonky formatting has to occur because stuff in tripple quotes appears literally
    def test_markdown_and_custom_formatting(self):
        test = """# This is some markdown
[d kicker]mixed in with some other stuff![/d]
let's hope it works"""

        result = u"<h1>This is some markdown</h1>\n\n<p><div class = \"kicker\">mixed in with some other stuff!</div>\nlet's hope it works</p>\n"

        self.assertEquals(post.evaluate(test), result)

    def test_multiple_classes(self):
        self.assertEquals(post.custom_markdown("[d test1 test2]"), '<div class = "test1 test2">')

    def test_make(self):
        input_directory = "test_env/test_input"
        output_directory = "test_env/test_output"

        # Make output if it doesn't already exist
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)

        if not os.path.exists(input_directory):
            self.fail("input directory must exist")

        architect.make("This is a test blog", input_directory, output_directory)

        # first, the files should now exist in the output directory
        self.assertTrue(os.path.exists(os.path.join(output_directory, 'steak.html')))
        self.assertTrue(os.path.exists(os.path.join(output_directory, 'bacon.html')))
        self.assertTrue(os.path.exists(os.path.join(output_directory, 'index.html')))

        # make sure there is a style folder
        self.assertTrue(os.path.exists(os.path.join(output_directory, 'style')))

        # test1 should have come before test2
        with open(os.path.join(output_directory, "index.html"), "r") as index:
            data = index.read()
            self.assertTrue(data.find("steak") > data.find("bacon"))


if __name__ == "__main__":
    unittest.main()
